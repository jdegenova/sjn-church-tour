
<?php $title = "Alter Furnishings: Church Tour"; ?>

<?php include 'include-head.php' ?> 
  <body>
    <div class="container homepage-nav">
      <ol class="breadcrumb reverse">
        <li><a href="index.php">Home</a></li>
        <li><a href="#" class="active">Altar Furnishings</a></li>
      </ol>

      <div class="row text-center nav-row">
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow fadeInLeft">
          <a href="altar-of-repose.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-altar-of-repose.jpg">
            <p class="nav-text btn btn-default btn-block">Altar of Repose</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInDown" data-wow-delay="0.3s">
          <a href="altar-of-sacrifice.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-altar-of-sacrifice.jpg">
            <p class="nav-text btn btn-default btn-block">Altar of Sacrifice</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInUp" data-wow-delay="0.1s">
          <a href="ambo.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-ambo.jpg">
            <p class="nav-text btn btn-default btn-block">Ambo</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInRight" data-wow-delay="0.2s">
          <a href="crucifix.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-crucifix.jpg">
            <p class="nav-text btn btn-default btn-block">Crucifix</p>
          </a>
        </div>
      </div>
      <div class="row text-center nav-row  wow bounceInUp">
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text">
          <a href="lamb-of-god-mural.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-lamb-of-god-mural.jpg">
            <p class="nav-text btn btn-default btn-block">Lamb of God Mural</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text">
          <a href="tabernacle.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-tabernacle.jpg">
            <p class="nav-text btn btn-default btn-block">Tabernacle</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text">
          <a href="triptych-joseph.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-statues-and-icons.jpg">
            <p class="nav-text btn btn-default btn-block">Triptych - Joseph</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text">
          <a href="triptych-mary.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-statues-and-icons.jpg">
            <p class="nav-text btn btn-default btn-block">Triptych - Mary</p>
          </a>
        </div>
      </div>
    </div>

    <?php include 'include-page-bottom-js.php' ?> 
  </body>
</html>