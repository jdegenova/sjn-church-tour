
<?php $title = "Alter Furnishings: Church Tour"; ?>

<?php include 'include-head.php' ?> 

  <body>
    <div class="container homepage-nav">
      <ol class="breadcrumb reverse">
        <li><a href="index.php">Home</a></li>
        <li><a href="altar-furnishings.php">Altar Furnishings</a></li>
        <li><a href="#" class="active">Altar of Repose</a></li>
      </ol>

      
        <div class="row">
          <div class="col-lg-12 page-title"><h1>Altar of Repose</h1></div>
        </div>
        <div class="row gallery">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img class="img-responsive img-center" src="images/altar-of-repose/altar-of-repose_1.jpg"/></div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img class="img-responsive img-center" src="images/altar-of-repose/altar-of-repose_2.jpg"/></div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img class="img-responsive img-center" src="images/altar-of-repose/altar-of-repose_3.jpg"/></div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img class="img-responsive img-center" src="images/altar-of-repose/altar-of-repose_4.jpg"/></div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            
            </div>
        </div>

      
        <div class="row reverse body-copy">
          <div class="col-lg-12 col-sm-12">
               <p>The Altar of Repose is also referred to as the Blessed Sacrament Altar.</p>

               
               <p>Later found in a bar. Rescued by Fr. Kevin Lutz, and restored by Henninger's in Cleveland.</p>
               
               <p>Blessed Sacrament Altar - Originally crafted in 1929 for solid oak for St. Henry Church in St. Louis, Missouri, 
                  which closed in the 70's.  Fr. Kevin Lutz of the Columbus Diocese rescued the altar from bar that sold alcohal 
                  and cigars and stored it for years in pieces at the Jubilee Museum.  Craftsmen from Heninger's in Cleveland 
                  restored the wood, fabricating missing pieces. 
               </p>

               <p>Artists from Conrad Schmidt hand painted the wood to look like stain, adding gilding and artwork to the 
                  appropriate masterpiece and focal point for our Lord in the tabernacle.  We are focused as a community 
                  on growing our relationship with Christ through a renewed devotion to our Lord in the Eucharist.  
                  Thus, we have placed at the physical center of our Church this beautiful altar in the hopes of helping 
                  each other place the Eucharist as the center of our worship and our lives. 
               <p>

               <p>A. Altar of Repose/Blessed Sacrament</p>

               <p>1. Candle of the Presence</p>

               <p>2. Tabernacle  Exodus 40:34-38</p>

               <p>3. Lamb of God  Revelation 5</p>

               <p>4. Alpha and Omega</p>
               
               <p>5. Wheat</p>
          </div>

      </div>
    </div>

    <?php include 'include-page-bottom-js.php' ?> 
 <!--bootstrap photo gallery-->

  </body>
</html>