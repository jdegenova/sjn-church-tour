
<?php $title = "Church Tour - Saint John Neumann - Sunbury Ohio"; ?>

<?php include 'include-head.php' ?> 
  <body>
    <div class="container homepage-nav">
      <ol class="breadcrumb reverse">
        <li><a href="#" class="active">Home</a></li>
      </ol>

      <div class="row text-center nav-row">
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInLeft">
          <a href="altar-furnishings.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-altar-furnishings.jpg">
            <p class="nav-text btn btn-default btn-block">Altar Furnishings</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInUp" data-wow-delay="0.2s">
          <a href="baptismal-font.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-baptismal-font.jpg">
            <p class="nav-text btn btn-default btn-block">Baptismal Font</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInDown" data-wow-delay="0.5s">
          <a href="eucharistic-adoration-chapel.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-eucharistic-adoration-chapel.jpg">
            <p class="nav-text btn btn-default btn-block">Eucharistic Adoration Chapel</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInRight" data-wow-delay="0.3s">
          <a href="gathering-space.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-gathering-space.jpg">
            <p class="nav-text btn btn-default btn-block">Gathering Space</p>
          </a>
        </div>
      </div>
      <div class="row text-center nav-row" >
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow slideInRight" data-wow-duration="1s" data-wow-delay="0.1s">
          <a href="stained-glass-windows.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-stained-glass-windows.jpg">
            <p class="nav-text btn btn-default btn-block">Stained Glass Windows</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow slideInRight" data-wow-duration="1s" data-wow-delay="0.2s">
          <a href="stations-of-the-cross.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-stations-of-the-cross.jpg">
            <p class="nav-text btn btn-default btn-block">Stations of the Cross</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow slideInRight" data-wow-duration="1s" data-wow-delay="0.3s">
          <a href="statues-and-icons.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-statues-and-icons.jpg">
            <p class="nav-text btn btn-default btn-block">Statues and Icons</p>
          </a>
        </div>
      </div>
    </div>
   
   
    <?php include 'include-page-bottom-js.php' ?> 
  </body>
</html>