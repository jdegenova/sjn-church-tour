
<?php $title = "Alter Furnishings: Church Tour"; ?>

<?php include 'include-head.php' ?> 
  <body>
    <div class="container homepage-nav">
      <ol class="breadcrumb">
        <li><a href="index.php">Home</a></li>
        <li><a href="#" class="active">Stained Glass Windows</a></li>
      </ol>

      <div class="row text-center nav-row">
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow fadeInLeft">
          <a href="rose-windows.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-rose-windows.jpg">
            <p class="nav-text btn btn-default btn-block">Rose Windows</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInDown" data-wow-delay="0.3s">
          <a href="sacrament-windows.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-sacrament-windows.jpg">
            <p class="nav-text btn btn-default btn-block">Sacrament Windows</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInUp" data-wow-delay="0.1s">
          <a href="joyful-mysteries.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-joyful-mysteries.jpg">
            <p class="nav-text btn btn-default btn-block">Joyful Mysteries</p>
          </a>
        </div>
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text wow bounceInRight" data-wow-delay="0.2s">
          <a href="luminous-mysteries.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-luminous-mysteries.jpg">
            <p class="nav-text btn btn-default btn-block">Luminous Mysteries</p>
          </a>
        </div>
      </div>
      <div class="row text-center nav-row  wow bounceInUp">
        <div class="col-lg-3 col-sm-6 text-center circle-nav-text">
          <a href="more-stained-glass.php">
            <img class="img-responsive img-circle center-block img-nav" src="images/navigation/nav-more-stained-glass.jpg">
            <p class="nav-text btn btn-default btn-block">More Stained Glass</p>
          </a>
        </div>
      </div>
    </div>

    <?php include 'include-page-bottom-js.php' ?> 
  </body>
</html>